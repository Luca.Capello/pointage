#!/bin/sh
#
# Simple CSV-based working-time tracking CLI software
# 
# Copyright (C) 2020-2023 Luca Capello <luca.capello@unige.ch>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# vars
LOG_DIR="${HOME}/var/log"
LOG_FILE="${LOG_DIR}/pointage.log"
STATUS=0
TIMEPOINTS=0
TODAY=$(date +%Y%m%d)
NOW=$(date +%s)
WORKING_DAY_STANDARD_HOURS=4


# functions
usage() {
    cat <<EOF
Usage: ${0} [-h] [-d TIME] [-g] [-l LOGFILE] [-n] [-s] [-w HOURS]

Options:
     -h : display this help and exit
     -d : override current time with TIME
     -g : get daily timepoints
     -l : override LOGFILE (defaults to \`${LOG_FILE}\`)
     -n : send graphical notification
     -s : show status
     -w : override daily working hours (defaults to 4)
EOF
}

format_time() {
    TIME_SECONDS=${1}
    TIME_SHOW_HOURS=''
    [ -n "${2}" ] && \
     TIME_SHOW_HOURS='$((%s/3600/24))-'

    EXTRA_NEGATIVE=''
    if [ "${TIME_SECONDS}" -lt 0 ]; then
        EXTRA_NEGATIVE='-'
        TIME_SECONDS="$(echo "${TIME_SECONDS}" | \
                         cut -c 2-)"
    fi

    ## <https://unix.stackexchange.com/questions/27013/displaying-seconds-as-days-hours-mins-seconds#338844>
    eval "echo ${EXTRA_NEGATIVE}$(date -ud "@${TIME_SECONDS}" +"${TIME_SHOW_HOURS}%H:%M:%S")"
}

daily_working_time() {
    WORKING_DATE=${1}

    POINTAGE_ORDER=0
    DAILY_WORKING_SECONDS=0
    ## <https://stackoverflow.com/questions/4198138/printing-everything-except-the-first-field-with-awk#7918051>
    for I in $(awk -F ',' "/${WORKING_DATE}/ {for (i=3; i<=NF; i++) print \$i}" "${LOG_FILE}"); do
        POINTAGE_ORDER=$(expr ${POINTAGE_ORDER} + 1)
        ## <https://stackoverflow.com/questions/15659848/how-do-i-check-whether-a-variable-has-an-even-numeric-value>
        if [ $(( ${POINTAGE_ORDER} % 2 )) -eq 0 ]; then
            POINTAGE_LAST="${I}"
            DAILY_WORKING_SECONDS=$(expr "${DAILY_WORKING_SECONDS}" \
                                         + \
                                         $(expr ${POINTAGE_LAST} \
                                                - \
                                                ${POINTAGE_FIRST}))
        else
            POINTAGE_FIRST="${I}"
        fi
    done
    if [ ${STATUS} -eq 1 ] && \
       [ $(( ${POINTAGE_ORDER} % 2 )) -ne 0 ]; then
        DAILY_WORKING_SECONDS=$(expr "${DAILY_WORKING_SECONDS}" \
                                     + \
                                     $(expr $(date +%s) \
                                            - \
                                            ${POINTAGE_FIRST}))
    fi

    format_time "${DAILY_WORKING_SECONDS}"
}

daily_extra_time() {
    WORKING_DATE=${1}
    TIME_NO_FORMAT=0
    [ -n "${2}" ] && \
     TIME_NO_FORMAT=1
    WORKING_DAY_STANDARD_SECONDS=$(expr 3600 \* ${WORKING_DAY_STANDARD_HOURS})
    [ -n "${3}" ] && \
     WORKING_DAY_STANDARD_SECONDS=0

    ## <https://stackoverflow.com/questions/1952404/linux-bash-multiple-variable-assignment#18218799>
    DAILY_EXTRA_SECONDS=$(daily_working_time "${WORKING_DATE}" | \
                           cut -d '-' -f 2 | \
                           sed -e 's/:/ /g' | \
                           { read WORKING_HOURS WORKING_MINUTES WORKING_SECONDS ; \
                             expr $(expr $(expr ${WORKING_HOURS} \* 3600) \
                                         + \
                                         $(expr ${WORKING_MINUTES} \* 60) \
                                         + \
                                         ${WORKING_SECONDS}) \
                                  - \
                                  ${WORKING_DAY_STANDARD_SECONDS}; }
                          )

    if [ ${TIME_NO_FORMAT} -eq 0 ]; then
        format_time "${DAILY_EXTRA_SECONDS}"
    else
        echo "${DAILY_EXTRA_SECONDS}"
    fi
}

previous_extra_time() {
    PREVIOUS_EXTRA_TIME="$(head -n -1 "${LOG_FILE}" | \
                            awk -F ',' '{s+=$4} END {print s}')"
    if [ -n "${1}" ]; then
        format_time "${PREVIOUS_EXTRA_TIME}" true
    else
        echo "${PREVIOUS_EXTRA_TIME}"
    fi
}

total_extra_time() {
    format_time "$(expr $(previous_extra_time) \
                        + \
                        $(daily_extra_time ${TODAY} true))" true
}

send_notification(){
    [ -n "${NOTIFICATION_CMD}" ] && \
     ${NOTIFICATION_CMD} "${0}" "${1}"
}


# checks
USER_TIME=''
NOTIFICATION_CMD=''
SKIP_LOG_DIR_CHECK=''
while getopts "hd:gl:nsw:" OPTION; do
    case $OPTION in
        h)
            usage
            exit 0
            ;;
        d)
            if [ -z "${OPTARG}" ]; then
                usage
                exit 1
            else
                USER_TIME="${OPTARG}"
            fi
            ;;
        g)
            TIMEPOINTS=1
            ;;
        l)
            if [ -z "${OPTARG}" ]; then
                usage
                exit 1
            else
                SKIP_LOG_DIR_CHECK='true'
                LOG_FILE="${OPTARG}"
            fi
            ;;
        n)
            NOTIFICATION_CMD="$(command -v notify-send)"
            if [ -z "${NOTIFICATION_CMD}" ]; then
                echo "E: notification command 'notify-send' not found in PATH."
                exit 1
            fi
            ;;
        s)
            STATUS=1
            ;;
        w)
            if ! expr "${OPTARG}" + 1 >/dev/null 2>&1; then
                echo "E: daily working hours '${OPTARG}' not a number or not an integer."
                exit 1
            else
                WORKING_DAY_STANDARD_HOURS=${OPTARG}
            fi
            ;;
        *)
            ;;
    esac
done
shift $((OPTIND - 1))

if [ -n "${1}" ]; then
    echo "E: too many arguments."
    usage
    exit 1
fi

if [ ${TIMEPOINTS} -eq 1 ] && \
   [ ${STATUS} -eq 1 ]; then
    echo "E: -g and -s are mutually exclusive."
    usage
    exit 1
fi

if [ -z "${SKIP_LOG_DIR_CHECK}" ]; then
    if [ ! -r "${LOG_DIR}" ]; then
        echo "E: ${LOG_DIR} is not readable or does not exist."
        exit 1
    elif [ ! -d "${LOG_DIR}" ]; then
        echo "E: ${LOG_DIR} is not a directory."
        exit 1
    fi
fi


# main
[ ! -r "${LOG_FILE}" ] && \
 echo "date,worked,extra,pointages" >"${LOG_FILE}"

if [ ${TIMEPOINTS} -eq 1 ]; then
    CSV_FIRST_FIELD=3
    if [ -n "${USER_TIME}" ]; then
        TODAY="$(date -d "${USER_TIME}" +%Y%m%d)"
        CSV_FIRST_FIELD=5
    fi
    NOTIFICATION_MSG="$(for I in $(awk -F ',' "/${TODAY}/ {for (i=${CSV_FIRST_FIELD}; i<=NF; i++) print \$i}" "${LOG_FILE}"); do
                            date -d "@${I}"
                         done)"
    echo "${NOTIFICATION_MSG}"
    send_notification "${NOTIFICATION_MSG}"
elif [ ${STATUS} -eq 1 ]; then
    NOTIFICATION_MSG="$(echo "\
I: today working time is $(daily_working_time ${TODAY}).
I: today extra time is $(daily_extra_time ${TODAY}).
I: previous extra time is $(previous_extra_time true).
I: total extra time is $(total_extra_time).")"
    echo "${NOTIFICATION_MSG}"
    send_notification "${NOTIFICATION_MSG}"
else
    if [ -n "${USER_TIME}" ]; then
        USER_TODAY="$(date -d "${USER_TIME}" +%Y%m%d)"
        if [ "${USER_TODAY}" != "${TODAY}" ]; then
            echo "E: user-provided date (${USER_TODAY}) is not today (${TODAY})."
            exit 1
        else
            NOW="$(date -d "${USER_TIME}" +%s)"
        fi
    fi

    NOTIFICATION_MSG="adding timepoint ${NOW}"
    send_notification "${NOTIFICATION_MSG}"
    logger -i -t "$0" "adding timepoint ${NOTIFICATION_MSG}"

    if grep -q -e "^${TODAY}" "${LOG_FILE}"; then
        sed -i -e "s/^\(${TODAY},.*\)$/\1,${NOW}/" "${LOG_FILE}"
    else
        ## "complete" the previous line
        LAST_WORKING_DATE_RAW="$(tail -n 1 "${LOG_FILE}" | \
                              cut -d ',' -f 1)"
        LAST_WORKING_DATE_HOLIDAY="$(tail -n 1 "${LOG_FILE}" | \
                                      grep -q -e ',h,' && \
                                      echo 'true')"
        sed -i -e "s/^\(${LAST_WORKING_DATE_RAW},[hw]\),\(.*\)$/\1,$(daily_working_time ${LAST_WORKING_DATE_RAW}),$(daily_extra_time ${LAST_WORKING_DATE_RAW} true ${LAST_WORKING_DATE_HOLIDAY}),\2/" "${LOG_FILE}"
        ## add the current line
        DAY_WEEK="$(date -d "${TODAY}" +%u)"
        if [ "${DAY_WEEK}" -ge 6 ]; then
            DAY_TYPE=h
        else
            DAY_TYPE=w
        fi
        echo "${TODAY},${DAY_TYPE},${NOW}" >>"${LOG_FILE}"
    fi
fi
